-- Copyright (C) 2020 Collabora Ltd
--   Author: Olivier Crete <olivier.crete@collabora.com>
-- Copyright (C) 2023 Sipradius LLC
--   Author: Gijs Peskens <gijs.peskens@sipradius.com>
-- Dissector for the RIST RTP extensions
-- Understands the RTP header extension from Main profile.
-- Also understands the RTCP extensions from both the Simple and the Main
-- profile.

local p_rtp_ext_rist = Proto("rtp.hdr_ext.rist",
			     "RIST RTP header extension");
local f_n = ProtoField.bool("rtp.hdr_ext.rist.n", "Null packet deletion",
			    8, nil, 0x80,
			    "Are there null MPEG-TS packets replaced by flags")
local f_e = ProtoField.bool("rtp.hdr_ext.rist.e",
			    "Sequence number extension present",
			    8, nil, 0x40,
			    "Is there a valid sequence number extension?")
local f_size = ProtoField.uint8("rtp.hdr_ext.rist.size",
				"Number of MPEG-TS packets in the original packet", base.DEC, nul, 0x38)
local f_t = ProtoField.bool("rtp.hdr_ext.rist.t",
			    "Size of the MPEG-TS packets", 8, {"188", "204"},
			    0x80, "False is 188 bytes, True is 204 bytes")
local f_npd_bits_1 = ProtoField.bool("rtp.hdr_ext.rist.npd_bit_1",
				     "MPEG-TS packet 1 removed",
				     8, nil, 0x40)
local f_npd_bits_2 = ProtoField.bool("rtp.hdr_ext.rist.npd_bit_2",
				     "MPEG-TS packet 2 removed",
				     8, nil, 0x20)
local f_npd_bits_3 = ProtoField.bool("rtp.hdr_ext.rist.npd_bit_3",
				     "MPEG-TS packet 3 removed",
				     8, nil, 0x10)
local f_npd_bits_4 = ProtoField.bool("rtp.hdr_ext.rist.npd_bit_4",
				     "MPEG-TS packet 4 removed",
				     8, nil, 0x08)
local f_npd_bits_5 = ProtoField.bool("rtp.hdr_ext.rist.npd_bit_5",
				     "MPEG-TS packet 5 removed",
				     8, nil, 0x04)
local f_npd_bits_6 = ProtoField.bool("rtp.hdr_ext.rist.npd_bit_6",
				     "MPEG-TS packet 6 removed",
				     8, nil, 0x02)
local f_npd_bits_7 = ProtoField.bool("rtp.hdr_ext.rist.npd_bit_7",
				     "MPEG-TS packet 7 removed",
				     8, nil, 0x01)
local f_seqnum_ext = ProtoField.uint16("rtp.hdr_ext.rist.seqnum_ext",
				       "RTP Sequence number extension",
				       base.DEC)

p_rtp_ext_rist.fields = {f_n, f_e, f_size, f_t, f_npd_bits_1, f_npd_bits_2,
			 f_npd_bits_3, f_npd_bits_4, f_npd_bits_5,
			 f_npd_bits_6, f_npd_bits_7, f_seqnum_ext}

function p_rtp_ext_rist.dissector(buf, pkt, root)
   root = root:add(p_rtp_ext_rist, buf:range())

   local byte0 = buf:range(0, 1)
   local byte1 = buf:range(1, 1)
   local size = byte0:bitfield(2, 3)

   root:add(f_n, byte0)
   root:add(f_e, byte0)
   if byte0:bitfield(0) == 1 then
      root:add(f_size, byte0)
      root:add(f_t, byte1)
      for i=1,size do
	 root:add(p_rtp_ext_rist.fields[i+4], byte1)
      end
   end

   if byte0:bitfield(1) == 1 then
      root:add(f_seqnum_ext, buf:range(2, 2))
   end
end


local rtp_hdr_table =  DissectorTable.get("rtp.hdr_ext")
rtp_hdr_table:add(0x5249, p_rtp_ext_rist)




local p_rtcp_app_rist = Proto("rtcp.app.rist",
			      "RIST RTCP APP packet");

local p_rtcp_app_rist_range_nack = Proto("rtcp.app.rist.range_nack",
					 "RIST RTCP APP Range NACK")
local f_rtcp_range_nack_start = ProtoField.uint16("rtcp.app.rist.range_nack.start",
						  "RTP seqnum of first missing packet",
						  base.DEC, nil, nil,
						  "RTP Sequence number of fiest missing packet")
local f_rtcp_range_nack_additional = ProtoField.uint16("rtcp.app.rist.range_nack.additional",
						       "Additional packets",
						       base.DEC, nil, nil,
						       "Number of additions missing packets after the first one")
p_rtcp_app_rist_range_nack.fields = {f_rtcp_range_nack_start,
				     f_rtcp_range_nack_additional}

local p_rtcp_app_rist_seqnum_ext = Proto("rtcp.app.rist.seqnum_ext",
					 "RIST RTCP APP Sequence number extension packet")
local f_rtcp_seqnum_ext = ProtoField.uint16("rtcp.app.rist.seqnum_ext.val",
					    "RTP Sequence number extension",
					    base.DEC)
p_rtcp_app_rist_seqnum_ext.fields = {f_rtcp_seqnum_ext}

local p_rtcp_app_rist_echo_request = Proto("rtcp.app.rist.echo_request", "RIST RTCP APP Echo Request extension packet")
local f_rtcp_app_echo_req_ts_msw = ProtoField.uint32("rtcp.app.rist.echo_request.ts_msw", "Timestamp, most significant word", base.DEC, nil, nil)
local f_rtcp_app_echo_req_ts_lsw = ProtoField.uint32("rtcp.app.rist.echo_request.ts_lsw", "Timestamp, least significant word", base.DEC, nil, nil)
p_rtcp_app_rist_echo_request.fields = { f_rtcp_app_echo_req_ts_msw, f_rtcp_app_echo_req_ts_lsw}

local p_rtcp_app_rist_echo_response = Proto("rtcp.app.rist.echo_response", "RIST RTCP APP Echo Response extension packet")
local f_rtcp_app_echo_resp_ts_msw = ProtoField.uint32("rtcp.app.rist.echo_response.ts_msw", "Timestamp, most significant word", base.DEC, nil, nil)
local f_rtcp_app_echo_resp_ts_lsw = ProtoField.uint32("rtcp.app.rist.echo_response.ts_lsw", "Timestamp, least significant word", base.DEC, nil, nil)
local f_rtcp_app_echo_resp_ts_delay = ProtoField.uint32("rtcp.app.rist.echo_response.ts_delay", "Processing delay (microseconds)", base.DEC, nil, nil)
p_rtcp_app_rist_echo_response.fields = {f_rtcp_app_echo_resp_ts_msw, f_rtcp_app_echo_resp_ts_lsw, f_rtcp_app_echo_resp_ts_delay}


function p_rtcp_app_rist.dissector(buf, pkt, root)

   local subtype = buf:range(0, 1):bitfield(3, 5)
   local length = buf:range(2, 2):uint()

   if subtype == 0 then
      local ranges = buf:range(12)
      local proto = root:add(p_rtcp_app_rist_range_nack, ranges)

      local ranges_tvb = ranges:tvb()
      for i=0,ranges:len()-4,4 do
		proto:add(f_rtcp_range_nack_start, ranges_tvb:range(i, 2))
		proto:add(f_rtcp_range_nack_additional, ranges_tvb:range(i + 2, 2))
      end

    elseif subtype == 1 then
      local proto = root:add(p_rtcp_app_rist_seqnum_ext, buf:range(12))
      if length == 3 then
		proto:add(f_rtcp_seqnum_ext, buf:range(12, 2))
      else
		proto:add_expert_info(PI_MALFORMED, PI_ERROR,
					"RIST APP subtype 1 (sequence  number extension) must have size 3")
      end
	elseif subtype == 2 then
	 local proto = root:add(p_rtcp_app_rist_echo_request, buf:range(12))
	 proto:add(f_rtcp_app_echo_req_ts_msw, buf:range(12, 4))
	 proto:add(f_rtcp_app_echo_req_ts_lsw, buf:range(16, 4))
	elseif subtype == 3 then
	 local proto = root:add(p_rtcp_app_rist_echo_response, buf:range(12))
	 proto:add(f_rtcp_app_echo_resp_ts_msw, buf:range(12, 4))
	 proto:add(f_rtcp_app_echo_resp_ts_lsw, buf:range(16, 4))
	 proto:add(f_rtcp_app_echo_resp_ts_delay, buf:range(20, 4))
   else
      root:add_expert_info(PI_MALFORMED, PI_ERROR,
			   string.format("Unknown RIST RTCP subtype %d", subtype))
   end
end



local rtcp_app_table = DissectorTable.get("rtcp.app.name")
rtcp_app_table:add("RIST", p_rtcp_app_rist)

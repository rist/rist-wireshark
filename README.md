# RIST Wireshark plugins

## GRE over UDP plugin

This Wireshark plugin dissects GRE over UDP protocol (RFC 8086), with GRE extensions defined by RIST main profile (VSF TR-06-02).
It works when GRE headers are not encrypted (no DTLS).

The filename is `gre_over_udp.lua`.

To use it:
- Install the lua file
- It opens an option of "GREoUDP" in the "Decode as..." dialog.
  - It arbitrarily register UDP port 1968 by default.

## RTP and RTCP extension plugin

This Wireshark plugin dissects the RTP and RTCP extensions defined in both the Simple Profile (VSF TR-06-01) and the Main Profile (VSF TR-06-02).

The filename is `rist-rtp.lua`.

## Installing

- Place the .lua files in the Wireshark program folder
- Include it at the end of `init.lua` with:
```
dofile(DATA_DIR.."gre_over_udp.lua")
dofile(DATA_DIR.."rist-rtp.lua")
```

Alternatively, on Linux systems, you can place the `.lua` files in
`~/.wireshark/plugins/` and on Windows,
you can place them in %APPDATA%\Wireshark\plugins and in both cases they will be automatically detected (no need to edit init.lua)

-- Zixi 2019
-- SipRadius 2020
-- Net Insight 2020
-- Sipradius 2023
-- explicit GRE protocol dissector - for GRE over UDP (rfc8086) and RIST main profile extensions

-- Generic GRE
local gre_over_udp = Proto("GREoUDP", "GRE protocol")

function gre_over_udp.dissector(buffer, pinfo, tree)
	local gre_dissector = DissectorTable.get("ip.proto"):get_dissector(47)
	-- pass it to the implicit GRE over IP dissector
	gre_dissector:call(buffer, pinfo, tree)
end

--libRIST experimental buffer negotiation
local librist_buffer_neg = Proto("libRIST_buffer_neg", "RIST libRIST Experimental buffer negotiation")
local librist_buffer_neg_sender_max = ProtoField.uint16("libRIST_buffer_neg.sender_max", "Sender Max buffer ms")
local librist_buffer_neg_receiver_cur = ProtoField.uint16("libRIST_buffer_neg.receiver_cur", "Receiver current buffer ms")
local librist_buffer_neg_prototype = ProtoField.uint16("libRIST_buffer_neg.prototype", "Proto type", base.HEX, {
		[0x0000] = "Global scope",
		[0x88B6] = "Reduced overhead flow scope",
		[0x0800] = "Full IP flow scope",
})
librist_buffer_neg.fields = { librist_buffer_neg_sender_max, librist_buffer_neg_receiver_cur, librist_buffer_neg_prototype}

function librist_buffer_neg.dissector(buffer, pinfo, tree)
	local subtree = tree:add(librist_buffer_neg, buffer(), "RIST libRIST experimental buffer negotiation message")
	subtree:add(librist_buffer_neg_sender_max, buffer(0, 2))
	subtree:add(librist_buffer_neg_receiver_cur, buffer(2, 2))
	subtree:add(librist_buffer_neg_prototype, buffer(4, 2))
	local prototype_val = buffer:range(4,2):uint()
	if prototype_val == 0x88B6 then
		local gre_keepalive_dissector = Dissector.get('gre_keepalive')
		gre_keepalive_dissector:call(buffer(6):tvb(), pinfo, tree)
	elseif prototype_val == 0x0800 then
		local ipdissector = Dissector.get('ip')
		ipdissector:call(buffer(6):tvb(), pinfo, tree)
	end
end

-- VSF EtherType
local vsf_ether = Proto("VSF_ether", "VSF Ethertype extension")
local vsf_type = ProtoField.uint16("vsf_ether.type", "Type", base.HEX, {[0x0000] = "RIST"})
local vsf_rist_subtype = ProtoField.uint16("vsf_ether.subtype", "Sub Type", base.HEX, {
		[0x0000] = "RIST Main Profile Reduced Overhead",
		[0x8000] = "RIST Main Profile Keep-Alive",
		[0x8001] = "RIST Main Profile Future Nonce Announcement",
		[0x8002] = "Rist libRIST experimental buffer negotiation",
})
vsf_ether.fields = {vsf_type, vsf_rist_subtype}

function vsf_ether.dissector(buffer, pinfo, tree)
	local subtree = tree:add(vsf_ether, buffer(), "VSF Ethertype extension Header")
	subtree:add(vsf_type, buffer(0, 2))
	local vsf_type_val = buffer:range(0, 2):uint()

	if vsf_type_val == 0x0000 then
		subtree:add(vsf_rist_subtype, buffer(2,2))
		local vsf_subtype_val = buffer:range(2,2):uint()
		if vsf_subtype_val == 0 then
			local gre_reduced_dissector = Dissector.get('gre_reduced')
			gre_reduced_dissector:call(buffer(4):tvb(), pinfo, tree)
		elseif vsf_subtype_val == 0x8000 then
			local gre_keepalive_dissector = Dissector.get('gre_keepalive')
			gre_keepalive_dissector:call(buffer(4):tvb(), pinfo, tree)
		elseif vsf_subtype_val == 0x8002 then
			local librist_buffer_neg_dissector = librist_buffer_neg.dissector
			librist_buffer_neg_dissector(buffer(4):tvb(), pinfo, tree)
		end
	end
end

-- Reduced GRE
local gre_reduced = Proto("GRE_reduced", "GRE reduced overhead")
local src_port = ProtoField.uint16("gre_reduced.src_port", "Source Port")
local dst_port = ProtoField.uint16("gre_reduced.dst_port", "Destination Port")
gre_reduced.fields = { src_port, dst_port }

function gre_reduced.dissector(buffer, pinfo, tree)
	local subtree = tree:add(gre_reduced,buffer(), "GRE reduced header")
	subtree:add(src_port, buffer(0,2))
	subtree:add(dst_port, buffer(2,2))
	if buffer:len() == 4 then
		return
	end
	-- pass payload to RTP/RTCP dissector
	local port = buffer:range(2,2):uint()
	if bit.band(port, 1) == 0 then
		local rtp_dissector = Dissector.get("rtp")
		rtp_dissector:call(buffer(4):tvb(), pinfo, tree)
	else
		local rtcp_dissector = Dissector.get("rtcp")
		rtcp_dissector:call(buffer(4):tvb(), pinfo, tree)
	end
end

-- Keepalive message
-- | ID(48) |X|R|B|A|P|E|L|N|D|T|V|J|F| Rsvd1(3) |
local gre_keepalive = Proto("GRE_keepalive", "GRE keepalive message header")
local id = ProtoField.ether("gre_keepalive.id", "Unique ID")
local X = ProtoField.uint16("gre_keepalive.X", "X", base.HEX, nil, 0x8000)
local R = ProtoField.uint16("gre_keepalive.R", "R", base.HEX, nil, 0x4000)
local B = ProtoField.uint16("gre_keepalive.B", "B", base.HEX, nil, 0x2000)
local A = ProtoField.uint16("gre_keepalive.A", "A", base.HEX, nil, 0x1000)
local P = ProtoField.uint16("gre_keepalive.P", "P", base.HEX, nil, 0x0800)
local E = ProtoField.uint16("gre_keepalive.E", "E", base.HEX, nil, 0x0400)
local L = ProtoField.uint16("gre_keepalive.L", "L", base.HEX, nil, 0x0200)
local N = ProtoField.uint16("gre_keepalive.N", "N", base.HEX, nil, 0x0100)
local D = ProtoField.uint16("gre_keepalive.D", "D", base.HEX, nil, 0x0080)
local T = ProtoField.uint16("gre_keepalive.T", "T", base.HEX, nil, 0x0040)
local V = ProtoField.uint16("gre_keepalive.V", "V", base.HEX, nil, 0x0020)
local J = ProtoField.uint16("gre_keepalive.J", "J", base.HEX, nil, 0x0010)
local F = ProtoField.uint16("gre_keepalive.F", "F", base.HEX, nil, 0x0008)
local Rsvd1 = ProtoField.uint16("gre_keepalive.Rsvd1", "Rsvd1", base.HEX, nil, 0x0007)
gre_keepalive.fields = { id,X,R,B,A,P,E,L,N,D,T,V,J,F,Rsvd1 }


local json_dissector = Dissector.get("json")
function gre_keepalive.dissector(buffer, pinfo, tree)
	local subtree = tree:add(gre_keepalive,buffer(), "GRE keepalive message")
	pinfo.cols.info:set("GRE keepalive")
	subtree:add(id, buffer(0,6))
	subtree:add(X, buffer(6,2))
	subtree:add(R, buffer(6,2))
	subtree:add(B, buffer(6,2))
	subtree:add(A, buffer(6,2))
	subtree:add(P, buffer(6,2))
	subtree:add(E, buffer(6,2))
	subtree:add(L, buffer(6,2))
	subtree:add(N, buffer(6,2))
	subtree:add(D, buffer(6,2))
	subtree:add(T, buffer(6,2))
	subtree:add(V, buffer(6,2))
	subtree:add(J, buffer(6,2))
	subtree:add(F, buffer(6,2))
	subtree:add(Rsvd1, buffer(6,2))

	if buffer:len() > 8 then
		json_dissector:call(buffer(8):tvb(), pinfo, tree)
	end
end

local rist_eap = Proto("rist_eap", "RIST EAP Packet")
local rist_eap_version = ProtoField.uint8("rist_eap.version", "EAP Version")
local rist_eap_type = ProtoField.uint8("rist_eap.type", "EAP Packet type", base.DEC, {[0] = "EAPOL-EAP", [1] = "EAPOL-Start", [2] = "EAPOL-Logoff"})
local rist_eap_length = ProtoField.uint16("rist_eap.length", "EAP Packet Payload length")
local rist_eapol_code = ProtoField.uint8("rist_eap.code", "EAPOL Code", base.DEC, {[1] = "Request", [2] = "Response", [3] = "Success", [4] = "Failure"})
local rist_eapol_identifier = ProtoField.uint8("rist_eap.identifier", "Identifier")
local rist_eapol_length = ProtoField.uint16("rist_eap.eapol_length", "EAPOL Length")
local rist_eapol_type = ProtoField.uint8("rist_eap.eapol_type", "EAPOL type", base.DEC, {[0x01] = "Identity", [0x03] = "Nak", [0x13] = "EAP SRP-SHA256"})
local rist_eapol_identity_data = ProtoField.string("rist_eap.eapol_identity_data", "Identity data")
local rist_eapol_nak_supported_type = ProtoField.uint8("rist_eap.eapol_nak_supported_type", "Supported type", base.DEC, {[0x13] = "EAP SRP-SHA256"})
local rist_eapol_request_subtype = ProtoField.uint8("rist_eap.eapol_request_subtype", "Request Subtype", base.DEC, {[0x01] = "Challenge", [0x02] = "Server key", [0x03] = "Server Validator", [0x10] = "Passphrase Request"})
local rist_eapol_request_server_name_len = ProtoField.uint16("rist_eap.eapol_request_servername_len", "Server Name Len")
local rist_eapol_request_server_name = ProtoField.string("rist_eap.eapol_request_servername", "Server Name")
local rist_eapol_request_salt_len = ProtoField.uint16("rist_eap.eapol_request_salt_len", "Salt Len")
local rist_eapol_request_salt = ProtoField.bytes("rist_eap.eapol_request_salt", "Salt")
local rist_eapol_request_generator_len = ProtoField.uint16("rist_eap.eapol_request_generator_len", "generator len")
local rist_eapol_request_generator = ProtoField.bytes("rist_eap.eapol_request_generator", "generator")
local rist_eapol_request_N = ProtoField.bytes("rist_eap.eapol_request_N", "N")
local rist_eapol_request_B = ProtoField.bytes("rist_eap.eapol.request_B", "B")
local rist_eapol_request_M2 = ProtoField.bytes("rist_eap.eapol.request_M2", "M2")
local rist_eapol_validator_reserved = ProtoField.uint24("rist_eap.eapol_validator_reserved", "Reserved")
local rist_eapol_validator_reserved2 = ProtoField.uint8("rist_eap.eapol_validator_reserved2", "Reserved", base.HEX, nil, 0xFE)
local rist_eapol_validator_U = ProtoField.uint8("rist_eap.eapol_validator_use_derived_key", "U", base.HEX, nil, 0x01)
local rist_eapol_response_subtype = ProtoField.uint8("rist_eap.eapol_response_subtype", "Response Subtype", base.DEC, {[0x01] = "Client Key", [0x02] = "Client Validator", [0x10] = "Passphrase Response"})
local rist_eapol_response_A = ProtoField.bytes("rist_eap.eapol.response_A", "A")
local rist_eapol_response_M1 = ProtoField.bytes("rist_eap.eapol.request_M1", "M1")
local rist_eapol_response_passphrase_reserved = ProtoField.uint8("rist_eap.eapol_passphrase_reserved", "Reserved", base.HEX, nil, 0x3F)
local rist_eapol_response_passphrase_H = ProtoField.uint8("rist_eap.eapol_passphrase_H", "H", base.HEX, {[0] = "AES-128", [1] = "AES-256"}, 0x40)
local rist_eapol_response_passphrase_U = ProtoField.uint8("rist_eap.eapol_passphrase_U", "U", base.HEX, nil, 0x80)
local rist_eapol_response_passphrase_encrypted_data = ProtoField.bytes("rist_eap.eapol_passphrase_encrypted_data", "Encrypted Passphrase")

rist_eap.fields = {rist_eap_version, rist_eap_type, rist_eap_length, rist_eapol_code, rist_eapol_identifier, rist_eapol_length, rist_eapol_type, rist_eapol_identity_data, rist_eapol_nak_supported_type,
	rist_eapol_request_subtype,
		--Challenge packet
		rist_eapol_request_server_name_len, rist_eapol_request_server_name, rist_eapol_request_salt_len, rist_eapol_request_salt, rist_eapol_request_generator_len, rist_eapol_request_generator, rist_eapol_request_N,
		--Server Key packet
		rist_eapol_request_B,
		--Both validator types
		rist_eapol_validator_reserved, rist_eapol_validator_reserved2,rist_eapol_validator_U,
		--Server validator
		rist_eapol_request_M2,
		--Passphrase request
	rist_eapol_response_subtype,
		--Client Key
		rist_eapol_response_A,
		--Client Validator
		rist_eapol_response_M1,
		--Passphrase response packet
		rist_eapol_response_passphrase_H, rist_eapol_response_passphrase_U, rist_eapol_response_passphrase_reserved, rist_eapol_response_passphrase_encrypted_data
}

function rist_eap.dissector(buffer, pinfo, tree)
	local subtree = tree:add(rist_eap, buffer(), "RIST EAP")
	subtree:add(rist_eap_version, buffer(0,1))
	local eap_type = buffer:range(1,1):uint()
	subtree:add(rist_eap_type, eap_type)
	local eap_length = buffer:range(2,2):uint()
	subtree:add(rist_eap_length, eap_length)

	if eap_type ~= 0 then
		if eap_length ~= 0 and (eap_type == 1 or eap_type == 2)  then
			proto:add_expert_info(PI_MALFORMED, PI_ERROR, "Start/Logoff MUST have length of 0")
		end
		return
	end

	local code = buffer:range(4, 1):uint()
	subtree:add(rist_eapol_code, code)
	subtree:add(rist_eapol_identifier, buffer(5, 1))
	local eapol_length = buffer:range(6,2):uint()
	subtree:add(rist_eapol_length, eapol_length)

	if eapol_length ~= eap_length then
		proto:add_expert_info(PI_MALFORMED, PI_ERROR, "EAP/EAPOL length mismatch")
	end
	if code ~= 1 and code ~= 2 then
		return
	end

	local eapol_type = buffer:range(8, 1):uint()
	subtree:add(rist_eapol_type, eapol_type)
	if eapol_type == 0x01 and code == 2 then
		subtree:add(rist_eapol_identity_data, buffer(9))
		return
	elseif eapol_type == 0x03 then
		subtree:add(rist_eapol_nak_supported_type, buffer(9, 1))
		return
	elseif eapol_type ~= 0x13 then
		return
	end

	if code == 1 then
		subtree:add(rist_eapol_request_subtype, buffer(9,1))
		local request_subtype_val = buffer:range(9,1):uint()
		if request_subtype_val == 0x01 then
			subtree:add(rist_eapol_request_server_name_len, buffer(10,2))
			local servername_len_val = buffer:range(10,2):uint()
			if servername_len_val > 0 then
				subtree:add(rist_eapol_request_server_name, buffer(12,servername_len_val))
			end
			local data_offset = 12 + servername_len_val
			subtree:add(rist_eapol_request_salt_len, buffer(data_offset, 2))
			local salt_len_val = buffer:range(data_offset, 2):uint()
			data_offset = data_offset + 2
			subtree:add(rist_eapol_request_salt, buffer(data_offset, salt_len_val))
			data_offset = data_offset + salt_len_val
			subtree:add(rist_eapol_request_generator_len, buffer(data_offset, 2))
			local generator_len_val = buffer:range(data_offset, 2):uint()
			data_offset = data_offset + 2
			if generator_len_val > 0 then
				subtree:add(rist_eapol_request_generator, buffer(data_offset, generator_len_val))
				subtree:add(rist_eapol_request_N, buffer(data_offset + generator_len_val))
			end
		elseif request_subtype_val == 0x02 then
			subtree:add(rist_eapol_request_B, buffer(10))
		elseif request_subtype_val == 0x03 then
			subtree:add(rist_eapol_validator_reserved, buffer(10, 3))
			subtree:add(rist_eapol_validator_reserved2, buffer(13, 1))
			subtree:add(rist_eapol_validator_U, buffer(13, 1))
			subtree:add(rist_eapol_request_M2, buffer(14))
		end
	else
		subtree:add(rist_eapol_response_subtype, buffer(9, 1))
		local request_subtype_val = buffer:range(9,1):uint()
		if request_subtype_val == 0x01 then
			subtree:add(rist_eapol_response_A, buffer(10))
		elseif request_subtype_val == 0x02 then
			subtree:add(rist_eapol_validator_reserved, buffer(10, 3))
			subtree:add(rist_eapol_validator_reserved2, buffer(13, 1))
			subtree:add(rist_eapol_validator_U, buffer(13, 1))
			subtree:add(rist_eapol_response_M1, buffer(14))
		elseif request_subtype_val == 0x10 then
			subtree:add(rist_eapol_response_passphrase_U, buffer(10, 1))
			subtree:add(rist_eapol_response_passphrase_H, buffer(10, 1))
			subtree:add(rist_eapol_response_passphrase_reserved, buffer(10, 1))
			local U_val = bit.band(buffer:range(10, 1), 7)
			if U_val == 0 then
				subtree:add(rist_eapol_response_passphrase_encrypted_data, buffer(11))
			end
		end
	end
end

-- register new GRE protocols
DissectorTable.get("gre.proto"):add(0x88B6, gre_reduced);
DissectorTable.get("gre.proto"):add(0x88B5, gre_keepalive);
DissectorTable.get("gre.proto"):add(0xCCE0, vsf_ether);
DissectorTable.get("gre.proto"):add(0x888E, rist_eap);
-- load the udp.port table
local udp_table = DissectorTable.get("udp.port")

-- register our protocol to handle UDP port 1968
udp_table:add(1968, gre_over_udp)

